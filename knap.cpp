#include<iostream>
#include<string>
#include<cstdlib>

using namespace std;

int arrw[100], arrb[100],a[100];
int item,maxwit,sumw,sumb,agn,i,j,k,l,m,as;

int main()
{
    cout<<"                              The Knapsack Problem\n"<<endl;

    cout<<"Enter Number of Item: "<<endl;
    cout<<"= ";cin>>item;
    cout<<"Enter Max Weight: "<<endl;
    cout<<"= ";cin>>maxwit;
    cout<<"Enter Weight Set: "<<endl;
    for(i = 0;i<item;i++)
	{
        cout<<"= ";cin>>arrw[i];
	}
	cout<<"Enter Benefit Set: "<<endl;
    for(j = 0;j<item;j++)
	{
        cout<<"= ";cin>>arrb[j];
	}
	cout<<"Enter Solution Set Only(1,0): "<<endl;
    for(m = 0;m<item;m++)
	{
        cout<<"= ";cin>>a[m];
        if(a[m] >1)
        {
            cout<<"*** Wrong Input Solution "<<a[m]<<", It Should Contain Only(1,0)"<<endl;
            --m;
            item+1;
        }
	}
	cout<<"======================================================= "<<endl;
	for(as=0,i=0,j=0;(as<item && i<item) && (as<item && j<item);as++,i++,j++)
    {
        k = arrw[i]/arrw[i];
        l = arrb[j]/arrb[j];
        if((k == a[as]) && (l == a[as]))
        {
            cout<<"Index: "<<i<<", Weight: "<<arrw[i]<<", Benefit: "<<arrb[j]<<", Solution: "<<a[as]<<endl;
            sumw = sumw + arrw[i];
            sumb = sumb + arrb[j];
        }
    }
    cout<<"======================================================= "<<endl;
    cout<<"Total Weight: "<<sumw<<endl;
    cout<<"Total Benefit: "<<sumb<<endl;
    cout<<"======================================================= "<<endl;
    cout<<endl;
    if(sumw > maxwit)
    {
        cout<<"Total Weight "<<sumw<<" Cross Max Weight Limit "<<maxwit<<" So This is Not a Perfect Solution."<<endl;
        cout<<"Total Benefit: "<<sumb<<endl;
    }
    else if(sumw < maxwit)
    {
        cout<<"Total Weight is "<<sumw<<" Within Weight Limit "<<maxwit<<" So This is Perfect Solution."<<endl;
        cout<<"Total Benefit: "<<sumb<<endl;
    }
    else if(sumw == maxwit)
    {
        cout<<"Total Weight is "<<sumw<<" Equal to Weight Limit "<<maxwit<<" So This is Perfect Solution."<<endl;
        cout<<"Total Benefit: "<<sumb<<endl;
    }
    else
    {
         cout<<"Logical Error!"<<endl;
    }
    cout<<endl;
    cout<<"======================================================= "<<endl;
    cout<<endl;

    cout<<"Want to Try Again:"<<endl;
    cout<<"1.Yes\n2.No"<<endl;
    cin>>agn;
    if(agn==1)
    {
        return main();
    }
    else if(agn==2)
    {
        exit(0);
    }
    cout<<endl;
}
